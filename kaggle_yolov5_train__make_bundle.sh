#!/usr/bin/env bash
set -e

if [ "$1" == "" ] || [ $# -gt 1 ]; then
    echo "[Kaggle Bundle] Parameter 1 is empty. It should be the run name."
    exit 1
fi
RUN_NAME=$1

echo "[Kaggle Bundle] Bundling yolov5 scripts into: '${RUN_NAME}'"

python "src/scripts/yolov5/make_bundle.py" bundle "${RUN_NAME}"

echo "[Kaggle Bundle] Done"