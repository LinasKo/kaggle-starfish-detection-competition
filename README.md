# Kaggle Competition: Starfish Detection
This is code to interact with the [Kaggle Starfish Competition](https://www.kaggle.com/c/tensorflow-great-barrier-reef).

The initial intention is to write the code locally, then use the Kaggle API to upload the notebooks to the site, and do the training there.

If multiple people join in, lets work in separate branches, and but merge common code into master.

⚠️ **Warning: Kaggle uses `Python 3.7`**

## Links
* [Competition Page](https://www.kaggle.com/c/tensorflow-great-barrier-reef)
* [Kaggle API Reference](https://www.kaggle.com/docs/api)
* [Python scripts (not notebooks) in Kaggle](https://www.kaggle.com/product-feedback/91185). Old link, but helps you get on track. Note that on initial test the script would fail for me randomly with various error like "os not defined" or something else stupid. Be careful and copy-paste into notebooks if it doesn't work.

## Folder Structure
Just see [this Medium article](https://medium.com/devcareers/cookie-cutter-organizing-data-science-projects-c3f13acd3247)

## Kaggle Working Structure
There is a way to make Kaggle kernels and push them via the command-lien interface, but it needs good understanding of the file paths. Sending in the probe gave the following results:

Working Dir: `/kaggle/working`

Structure of `/kaggle`:

```
/kaggle
  /src
    script.py
    __script__.ipynb
  /lib
    /kaggle
      gcp.py
  /input
    /tensorflow-great-barrier-reef
      /greatbarrierreef
        __init__.py
        competition.cpython-37m-x86_64-linux-gnu.so
      /train_images
        /video_1
          ... (jpg files)
        /video_2
          ... (jpg files)
        /video_0
          ... (jpg files)
      example_sample_submission.csv
      example_test.npy
      train.csv
      test.csv
  /working
```

## Uploading multiple files
Kaggle seems to bundle it into one script.

But it is possible to upload it as a dataset: first cell of [This Notebook](https://www.kaggle.com/rtatman/reproducing-research-men-also-like-shopping)

Also [here](https://www.kaggle.com/c/recommender-system-2018-challenge-polimi/discussion/69529).

Hmm...

### Upload codeset example:

```bash
KAGGLE_USER="mnephisto"
RUN_NAME="cots-hunt--test-bundle-1"

# First time:
kaggle datasets create -p "kaggle-bundles/${RUN_NAME}/codeset/" --dir-mode zip

# Thereafter:
kaggle datasets version -m "codeset" -p "kaggle-bundles/${RUN_NAME}/codeset/" --dir-mode zip --delete-old-versions

# Check status:
kaggle datasets status "${KAGGLE_USER}/${RUN_NAME}"
```

### Upload kernel example:
```bash
KAGGLE_USER="mnephisto"
RUN_NAME="cots-hunt--test-bundle-1"

kaggle kernels push -p "kaggle-bundles/${RUN_NAME}/kernel/"
```