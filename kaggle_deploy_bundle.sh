#!/usr/bin/env bash
set -e

if [ "$1" == "" ] || [ $# -gt 1 ]; then
    echo "[Kaggle Deploy] Parameter 1 is empty. It should be the bundle name."
    exit 1
fi
BUNDLE_NAME=$1

echo "[Kaggle Deploy] Sending bundle to kaggle: '${BUNDLE_NAME}'"

python "src/scripts/kaggle-api/push_bundle.py" all "${BUNDLE_NAME}"

echo "[Kaggle Deploy] Deployed."