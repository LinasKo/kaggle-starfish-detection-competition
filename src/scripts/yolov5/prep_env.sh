#!/usr/bin/env bash
set -e

YOLOv5_DIR="/kaggle/working/frameworks/yolov5"

# Prepare environment
echo '[Prepare Environment] Starting'
pip install --user -q toml joblib pandas pyyaml typer torch opencv-python wandb

mkdir -p "/kaggle/data/interim"
mkdir -p "/kaggle/data/processed"

mkdir -p "/kaggle/working/frameworks"

git clone "https://github.com/ultralytics/yolov5" "${YOLOv5_DIR}"
cd "${YOLOv5_DIR}"
pip install -qr "requirements.txt"

echo '[Prepare Environment] Done.'
