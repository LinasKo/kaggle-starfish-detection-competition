import typer
import re
import json
import shutil
from colorama import Fore, Style
import os


if os.environ.get("KAGGLE_KERNEL_RUN_TYPE"):
    raise RuntimeError("This script should not be run from within a kernel")

from src.utils.local_paths import PATHS
from src.scripts.clean import clean_bundle, clean_kernel, clean_codeset


app = typer.Typer()

USERNAME = "mnephisto"
COMPETITION_NICKNAME = "cots-hunt"


@app.command("bundle")
def make_bundle(run_name: str, with_gpu: bool = False):
    _assert_valid_run_name(run_name)
    full_name = _full_run_name(run_name)

    # Create folders
    clean_bundle(full_name)
    BUNDLE_PATH = PATHS.BUNDLES / full_name
    BUNDLE_PATH.mkdir(parents=True, exist_ok=False)

    make_codeset(run_name)
    make_kernel(run_name, with_gpu)

    print(f" [{Fore.GREEN}✓{Fore.RESET}] Bundle {Style.BRIGHT}{full_name}{Style.NORMAL} created.")
    print(f"     Location: {BUNDLE_PATH}")
    print()


@app.command("kernel")
def make_kernel(run_name: str, with_gpu: bool = False):
    _assert_valid_run_name(run_name)
    full_name = _full_run_name(run_name)

    clean_kernel(full_name)
    KERNEL_PATH = PATHS.BUNDLES / full_name / "kernel"
    KERNEL_PATH.mkdir(parents=True, exist_ok=False)

    # Script
    train_template_file = PATHS.SOURCE / "scripts" / "yolov5" / "main.py.incomplete"
    with open(train_template_file, "r") as f:
        train_script_text = f.read()
        train_script_text = train_script_text.replace(
            "<CODESET_FULL_NAME>", full_name)
    with open(KERNEL_PATH / "main.py", "w") as f:
        f.write(train_script_text)

    # Metadata
    metadata = {
        "title": f"{full_name}-kernel",
        "id": f"{USERNAME}/{full_name}-kernel",
        "code_file": f"main.py",
        "language": "python",
        "kernel_type": "script",
        "is_private": True,
        "enable_gpu": with_gpu,
        "enable_internet": True,
        "dataset_sources": [f"{USERNAME}/{full_name}-codeset"],
        "competition_sources": ["tensorflow-great-barrier-reef"],
        "kernel_sources": [],
    }

    dst = KERNEL_PATH / "kernel-metadata.json"
    with open(dst, "w") as fh:
        fh.write(f"{json.dumps(metadata, indent=2)}")

    print(f" [{Fore.GREEN}✓{Fore.RESET}] Kernel {Style.BRIGHT}{full_name}{Style.NORMAL} created.")
    print(f"     Location: {KERNEL_PATH}")
    print()


@app.command("codeset")
def make_codeset(run_name: str):
    """
    Send code as a dataset.

    Kaggle does not allow sending multiple code files to the kernel. Send it as a dataset.
    """
    _assert_valid_run_name(run_name)
    full_name = _full_run_name(run_name)

    clean_codeset(full_name)
    CODESET_PATH = PATHS.BUNDLES / full_name / "codeset"
    CODESET_PATH.mkdir(parents=True, exist_ok=False)

    # Metadata
    metadata = {
        "title": f"{full_name}-codeset",
        "id": f"{USERNAME}/{full_name}-codeset",
        "licenses": [{"name": "unknown"}]
    }

    dst = CODESET_PATH / "dataset-metadata.json"
    with open(dst, "w") as fh:
        fh.write(f"{json.dumps(metadata, indent=2)}")

    # Model-specific
    for fname in ["__init__.py", "prep_env.sh", "make_data.py", "train.sh"]:
        src = PATHS.SOURCE / "scripts" / "yolov5" / fname
        dst = CODESET_PATH / fname
        shutil.copy(src, dst)

    # Dependencies
    for fname in ["bundle_paths.py"]:
        src = PATHS.SOURCE / "utils"
        dst = CODESET_PATH / "utils"
        shutil.copytree(src, dst)

    print(f" [{Fore.GREEN}✓{Fore.RESET}] Codeset {Style.BRIGHT}{full_name}{Style.NORMAL} created.")
    print(f"     Location: {CODESET_PATH}")
    print()


def _full_run_name(run_name):
    full_name = f"{COMPETITION_NICKNAME}-{run_name}"
    if 6 > len(full_name) > 50:
        raise RuntimeError(
            "Full run name must be between 6 and 50 characters long")

    return full_name


def _assert_valid_run_name(run_name):
    if not re.match(r'^[a-zA-Z0-9_\-]+$', run_name):
        raise RuntimeError(
            "Run name can only contain letters, numbers and dashes")

    # try making a full name, in case it's not a valid one
    _full_run_name(run_name)


if __name__ == '__main__':
    app()
    print(f"{Fore.YELLOW}Don't forget to check the metadata and configs before pushing the bundle!{Fore.RESET}")
