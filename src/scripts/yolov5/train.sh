#!/usr/bin/env bash
set -e

if [ "$1" == "" ] || [ $# -gt 1 ]; then
    echo "[Kaggle Deploy] Parameter 1 is empty. It should be the bundle name."
    exit 1
fi
BUNDLE_NAME=$1

# Training Params
IMG_SIZE=1280   # Always set to 1280 for normal runs
BATCH=16        # (yolov5s) Can handle, maybe 9 on CPU, but likely more on GPU
EPOCHS=10

# Paths
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
YOLOv5_DIR="/kaggle/working/frameworks/yolov5"
DATA_YAML="/kaggle/data/processed/dataset.yaml"

# WandB
WANDB_ENTITY="much-vision"
WANDB_PROJ_NAME="${BUNDLE_NAME}"

# Train
echo "[Train] Starting"
python "${YOLOv5_DIR}/train.py" \
    --img "${IMG_SIZE}" \
    --batch "${BATCH}" \
    --epochs "${EPOCHS}" \
    --data "${DATA_YAML}" \
    --weights "yolov5s.pt" \
    --entity "${WANDB_ENTITY}" \
    --project "${WANDB_PROJ_NAME}" \
    --save-period 1 \
    --bbox_interval 10

    # --upload_dataset \   # Likely gives issues: "ValueError: Cannot add the same path twice: data/images/629.jpg "

echo "[Train] Done"
