from pathlib import Path
import pandas as pd
import shutil
import yaml
from ast import literal_eval
from joblib import Parallel, delayed

import os
if os.environ.get("KAGGLE_KERNEL_RUN_TYPE"):
    from utils.bundle_paths import PATHS
else:
    from src.utils.local_paths import PATHS


IMG_W = 1280
IMG_H = 720

VALID_SEQUENCES = [8503, 26651, 996, 59337]
# TRAIN_SEQUENCES = []  # All not in valid

EXCLUDE_NO_LABELS = True
PRESERVE_SPACE = True


def make_dataset(exclude_without_labels=False):
    """Perform all actions to convert from raw to processed data ready for training YOLOv5"""
    train_df = pd.read_csv(PATHS.TRAIN_CSV)
    if exclude_without_labels:
        exclude_with_no_labels(train_df)
    split_by_sequence(train_df)
    augment_train_pd(train_df)
    save_train_csv(train_df)
    return train_df


def exclude_with_no_labels(train_df):
    """Remove rows with no labels"""
    num_rows_with_no_labels = len(train_df[train_df.annotations == "[]"])
    train_df.drop(train_df[train_df.annotations == "[]"].index, inplace=True)
    print(f"Dropped {num_rows_with_no_labels} rows without annotations.")


def split_by_sequence(train_df):
    """Read raw data and split into sequences"""

    # Prepare new paths
    print("Adding column 'file_path'")
    train_df["file_path"] = str(PATHS.INTERIM_DATA) + "/" + "sequence_" + \
        train_df["sequence"].astype(
        str) + "/" + train_df["sequence_frame"].astype(str) + ".jpg"

    def copy_out_img(row):
        vid_id = row["video_id"]
        vid_frame = row["video_frame"]
        raw_path = PATHS.TRAIN_IMAGES / f"video_{vid_id}" / f"{vid_frame}.jpg"
        new_path = Path(row["file_path"])

        new_path.parent.mkdir(parents=True, exist_ok=True)

        if not new_path.is_file():
            shutil.copy(raw_path, new_path)
            new_path.chmod(0o666)

    Parallel(n_jobs=-1)(delayed(copy_out_img)(row)
                        for _, row in train_df.iterrows())


def augment_train_pd(train_df):
    """Augment the training dataframe with anything useful"""

    # Count detections
    print("Adding column 'detection_count'")
    if "detection_count" not in train_df.columns:
        det_counts = train_df.apply(
            lambda row: len(literal_eval(row.annotations)), axis=1)
        train_df["detection_count"] = det_counts


def save_train_csv(train_df):
    """Save the training dataframe as a CSV"""
    csv_target = PATHS.INTERIM_DATA / "train.csv"
    train_df.to_csv(csv_target, index=False)
    print(f"Saved to new CSV: {csv_target}")


def to_yolo_format(train_df, move_files=False):
    """Convert the training dataframe to YOLO format"""
    print("\nConverting to YOLO format")

    def to_yolo_folders(seq_dir_name, move_files=False):
        seq_path = Path(seq_dir_name)
        seq_name = seq_path.name

        # Create folders
        label_path = PATHS.PROCESSED_DATA / seq_name / "labels"
        label_path.mkdir(parents=True, exist_ok=True)

        # Move files
        imgs_path = PATHS.PROCESSED_DATA / seq_name / "images"
        if move_files:
            shutil.move(str(seq_path), str(imgs_path))
        else:
            shutil.copytree(str(seq_path), str(imgs_path))

    Parallel(n_jobs=-1)(delayed(lambda x: (to_yolo_folders(x, move_files=move_files)))(seq_dir_name)
                        for seq_dir_name in PATHS.INTERIM_DATA.glob("sequence_*"))

    # Create labels
    print("Creating labels")

    def create_label(row):
        file_path = Path(row["file_path"])
        annotations = literal_eval(row["annotations"])

        label_path = PATHS.PROCESSED_DATA / file_path.parent.name / \
            "labels" / (file_path.stem + ".txt")

        label_content = ""
        class_id = 0
        for ann in annotations:
            ann_x = ann["x"]
            ann_y = ann["y"]
            ann_w = ann["width"]
            ann_h = ann["height"]

            ann_norm_cx = (ann_x + ann_w / 2) / IMG_W
            ann_norm_cy = (ann_y + ann_h / 2) / IMG_H
            ann_norm_w = ann_w / IMG_W
            ann_norm_h = ann_h / IMG_H

            label_content += f"{class_id} {ann_norm_cx} {ann_norm_cy} {ann_norm_w} {ann_norm_h}\n"

        if not label_path.is_file():
            with open(label_path, "w") as f:
                f.write(label_content)

    Parallel(n_jobs=-1)(delayed(create_label)(row)
                        for _, row in train_df.iterrows())

    # Make yaml file
    yaml_path = PATHS.PROCESSED_DATA / "dataset.yaml"
    train_seqs = [seq_num for seq_num in train_df["sequence"].unique(
    ) if seq_num not in VALID_SEQUENCES]
    train_seq_paths = [str(PATHS.PROCESSED_DATA /
                           f"sequence_{seq_num}") for seq_num in train_seqs]
    valid_seq_paths = [str(PATHS.PROCESSED_DATA /
                           f"sequence_{seq_num}") for seq_num in VALID_SEQUENCES]

    with open(yaml_path, "w") as f:
        f.write(yaml.dump({
            "train": train_seq_paths,
            "val": valid_seq_paths,
            "nc": 1,
            "names": ["*"]
        }))


if __name__ == '__main__':
    print("[Make Data] Starting")
    train_df = make_dataset(exclude_without_labels=EXCLUDE_NO_LABELS)
    to_yolo_format(train_df, move_files=PRESERVE_SPACE)
    print("[Make Data] Done")
