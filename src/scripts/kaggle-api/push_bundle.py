import os
import typer
import subprocess
from time import sleep
from colorama import Fore, Style

if os.environ.get("KAGGLE_KERNEL_RUN_TYPE"):
    from utils.bundle_paths import PATHS
else:
    from src.utils.local_paths import PATHS

app = typer.Typer()

LN = f"[{Fore.CYAN}Push Bundle{Fore.RESET}]"
KAGGLE_USERNAME = "mnephisto"


@app.command("all")
def push_all(bundle_name):
    assert (PATHS.BUNDLES /
            bundle_name).is_dir(), f"Bundle {bundle_name} does not exist"

    push_codeset(bundle_name)
    push_kernel(bundle_name)


@app.command("codeset")
def push_codeset(bundle_name: str):
    """
    Push the codeset from the kaggle-bundles to kaggle

    :bundle_name: The full name of the bundle
    """
    assert (PATHS.BUNDLES /
            bundle_name).is_dir(), f"Bundle {bundle_name} does not exist"

    CODESET_ID = f"{KAGGLE_USERNAME}/{bundle_name}-codeset"
    CODESET_PATH = PATHS.BUNDLES / bundle_name / "codeset"
    print(
        f"{LN} Pushing codeset {Style.BRIGHT}{CODESET_ID}{Style.NORMAL}")

    codeset_exists, codeset_ready = _codeset_status(CODESET_ID)

    if not codeset_exists:
        print(f"{LN} New codeset: pushing.")
        subprocess.run([
            "kaggle", "datasets", "create",
            "-p", str(CODESET_PATH),
            "--dir-mode", "zip",
            "--quiet"])

    else:
        print(f"{LN} Previous codeset found: updating.")
        while not codeset_ready:
            print(
                f"{LN} Waiting for previous codeset to be ready")
            sleep(1)
            codeset_exists, codeset_ready = _codeset_status(CODESET_ID)
            assert codeset_exists, "Previous codeset deleted while pushing version?"

        subprocess.run([
            "kaggle", "datasets", "version",
            "-m", "codeset update",
            "-p", str(CODESET_PATH),
            "--dir-mode", "zip",
            "--quiet"])

    sleep(2)
    codeset_exists, codeset_ready = _codeset_status(CODESET_ID)
    if not codeset_exists:
        raise Exception(f"Failed to create {CODESET_ID}")
    while not codeset_ready:
        sleep(1)
        codeset_exists, codeset_ready = _codeset_status(CODESET_ID)

    print(f"{LN} Codeset ready.\n")


@app.command("kernel")
def push_kernel(bundle_name: str):
    """
    Push the kernel from the kaggle-bundles to kaggle

    :bundle_name: The full name of the bundle
    """
    assert (PATHS.BUNDLES /
            bundle_name).is_dir(), f"Bundle {bundle_name} does not exist"

    KERNEL_ID = f"{KAGGLE_USERNAME}/{bundle_name}-kernel"
    KERNEL_PATH = PATHS.BUNDLES / bundle_name / "kernel"
    print(
        f"{LN} Pushing kernel {Style.BRIGHT}{KERNEL_ID}{Style.NORMAL}")

    subprocess.run(
        ["kaggle", "kernels", "push", "-p", str(KERNEL_PATH)])

    print(f"{LN} Pushed.\n")


def _codeset_status(codeset_id: str):
    process = subprocess.Popen(
        ["kaggle", "datasets", "status", codeset_id], stdout=subprocess.PIPE)
    resp_text = process.communicate()[0].decode("utf-8")
    codeset_exists = resp_text != "404 - Not Found\n"
    codeset_ready = resp_text == "ready"
    return codeset_exists, codeset_ready


if __name__ == '__main__':
    app()
