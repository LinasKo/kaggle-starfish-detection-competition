from pathlib import Path
import shutil
import typer

from src.utils.local_paths import PATHS


KEPT_FILES = [".gitkeep"]
app = typer.Typer()


@app.command("all")
def clean_all():
    clean_data()
    clean_frameworks()
    clean_bundles()


@app.command("data")
def clean_data():
    _remove_from_dir(PATHS.INTERIM_DATA)
    _remove_from_dir(PATHS.PROCESSED_DATA)


@app.command("frameworks")
def clean_frameworks():
    _remove_from_dir(PATHS.FRAMEWORKS)


@app.command("bundles")
def clean_bundles():
    _remove_from_dir(PATHS.BUNDLES)


@app.command("bundle")
def clean_bundle(bundle_name):
    _remove_dir(PATHS.BUNDLES / bundle_name)


@app.command("codeset")
def clean_codeset(bundle_name):
    _remove_dir(PATHS.BUNDLES / bundle_name / "codeset")


@app.command("kernel")
def clean_kernel(bundle_name):
    _remove_dir(PATHS.BUNDLES / bundle_name / "kernel")


def _remove_from_dir(dir_name):
    path = Path(dir_name)
    for file in path.iterdir():
        if file not in KEPT_FILES:
            _remove_any(file)


def _remove_dir(dir_name):
    path = Path(dir_name)
    if path.is_dir():
        shutil.rmtree(path)


def _remove_any(path):
    if path.is_dir():
        shutil.rmtree(path)
    else:
        path.unlink()


if __name__ == "__main__":
    app()
