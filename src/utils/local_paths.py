from pathlib import Path
import toml


def find_root(limit=10):
    path = Path(__file__).absolute().parent

    for _ in range(limit):
        for file in path.iterdir():
            if file.name == "pyproject.toml":
                project_info = toml.load(path / "pyproject.toml")
                if project_info["tool"]["poetry"]["name"] == "kaggle-starfish-detection-competition":
                    return path
        path = path.parent
    else:
        raise Exception("Could not find project root")


class PATHS:
    """A static class, containing knowledge of where to find various local files"""

    # Fixed paths (any competition)
    PROJ_ROOT = find_root()
    RAW_DATA = PROJ_ROOT / "data" / "raw"

    INTERIM_DATA = PROJ_ROOT / "data" / "interim"
    PROCESSED_DATA = PROJ_ROOT / "data" / "processed"

    MODELS = PROJ_ROOT / "models"
    SOURCE = PROJ_ROOT / "src"
    NOTEBOOKS = SOURCE / "notebooks"
    FRAMEWORKS = SOURCE / "frameworks"
    SCRIPTS = SOURCE / "scripts"
    UTILS = SOURCE / "utils"
    BUNDLES = PROJ_ROOT / "kaggle-bundles"

    # Useful libs
    WANDB = FRAMEWORKS / "wandb"

    # COTS competition-specific
    TRAIN_CSV = RAW_DATA / "tensorflow-great-barrier-reef" / "train.csv"
    TRAIN_IMAGES = RAW_DATA / "tensorflow-great-barrier-reef" / "train_images"
    TEST_CSV = RAW_DATA / "tensorflow-great-barrier-reef" / "test.csv"

    YOLOv5 = FRAMEWORKS / "yolov5"

    def __init__(self):
        raise Exception("This is a static class. No initialisation, please!")
