from pathlib import Path


class PATHS:
    """A static class, containing knowledge of where to find various local files"""

    # Fixed paths (any competition)
    PROJ_ROOT = Path("/kaggle")
    RAW_DATA = Path("/kaggle/input/")

    INTERIM_DATA = PROJ_ROOT / "data" / "interim"
    PROCESSED_DATA = PROJ_ROOT / "data" / "processed"

    SOURCE = PROJ_ROOT / "code"
    FRAMEWORKS = PROJ_ROOT / "working" / "frameworks"
    UTILS = SOURCE / "utils"
    BUNDLES = PROJ_ROOT / "kaggle-bundles"

    # Useful libs
    WANDB = FRAMEWORKS / "wandb"

    # COTS competition-specific
    TRAIN_CSV = RAW_DATA / "tensorflow-great-barrier-reef" / "train.csv"
    TRAIN_IMAGES = RAW_DATA / "tensorflow-great-barrier-reef" / "train_images"
    TEST_CSV = RAW_DATA / "tensorflow-great-barrier-reef" / "test.csv"

    YOLOv5 = FRAMEWORKS / "yolov5"

    def __init__(self):
        raise Exception("This is a static class. No initialisation, please!")
